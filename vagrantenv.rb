#!/usr/bin/env ruby

require 'yaml'

class VagrantEnv

  def initialize(config_yml='environment.yml')
    @vars = YAML.load_file(config_yml)
  end

  # security

  def ssh_security_enabled?()
    not @vars['security']['ssh_key_file'].nil?
  end

  def ssh_identity_key_file()
    @vars['security']['ssh_key_file']
  end

  # end security

  def ip(server)
    @vars['servers'][server]['ip']
  end

  def ext_ip(server)
    @vars['servers'][server]['ext_ip']
  end

  def ext_iface(server)
    @vars['servers'][server]['ext_iface']
  end

  def ext_gw()
    @vars['ext_gw']
  end

  def hostname(server)
    @vars['servers'][server]['hostname']
  end

  def nodes()
    @vars['nodes']
  end

  def aliases(server)
    ['%s.%s' %[@vars['servers'][server]['hostname'],@vars['domain']]]
  end

end
